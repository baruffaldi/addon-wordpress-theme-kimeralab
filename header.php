<!--
$Id: index.php,v 0.0.0.1 22/04/2006 02:02:07 mdb Exp $
$Author: mdb $

www.be-you.org Index Script

Copyright Kimera Team (c) 2006

You may not reproduce it elsewhere without the prior written permission of the author.
However, feel free to study the code and use techniques you learn from it elsewhere.
-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head profile="http://gmpg.org/xfn/11">
			<title><?php bloginfo('name'); wp_title(); ?></title>
			<meta name='metadata'   content='Be-You :: Young Festival' />
			<meta name='subject'    content='Festival dell` arte in Liguria' />
			<meta name='author'     content='Kimera Team for Be-You' />
			<meta name='publisher'  content='www.kimera-lab.com' />
			<meta name='date'       content='30apr2006' />
			<meta name='form'       content='xhtml' />
			<meta name="Copyright" content="Copyright (c) 2006 Be-You :: Young Festival" />
			<meta http-equiv='Keywords' content='Be-you, Festival, Liguria, Genova' />
			<meta http-equiv='Description' content='Festival dell` arte in Liguria' />
			<meta http-equiv="Content-Type" content="<?php bloginfo('charset'); ?>" />
			<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
			<link rel="shortcut icon" href="http://alvinwoon.com/blog/wp-content/themes/stand/favicon.ico" />
			<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen, projection" />
			<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
			<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
			<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
			<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
			<?php wp_head();?>
	</head>

	<body>

		<div id="container">
		
			<div id="header"></div>