    <div id="navigation">
		<li<?php if (is_home()) echo " class=\"selected\""; ?>><a href="<?php bloginfo('url'); ?>">Home</a></li>
		<?php
		$pages = BY_get_pages();
		if ($pages) {
			foreach ($pages as $page) {
				$page_id = $page->ID;
   				$page_title = $page->post_title;
   				$page_name = $page->post_name;
   				if ($page_name == "archives") {
   					(is_page($page_id) || is_archive() || is_search() || is_single())?$selected = ' class="selected"':$selected='';
   					echo "<li".$selected."><a href=\"".get_page_link($page_id)."\">Archives</a></li>\n";
   				}
   				elseif($page_name == "about") {
   					(is_page($page_id))?$selected = ' class="selected"':$selected='';
   					echo "<li".$selected."><a href=\"".get_page_link($page_id)."\">About</a></li>\n";
   				}
   				elseif ($page_name == "contact") {
   					(is_page($page_id))?$selected = ' class="selected"':$selected='';
   					echo "<li".$selected."><a href=\"".get_page_link($page_id)."\">Contact</a></li>\n";
   				}
				elseif ($page_name == "the-wall") {
   					(is_page($page_id))?$selected = ' class="selected"':$selected='';
   					echo "<li".$selected."><a href=\"http://www.be-you.org/thewall.php\">The Wall</a></li>\n";
   				}
				elseif ($page_name == "jukebox") {
   					(is_page($page_id))?$selected = ' class="selected"':$selected='';
   					echo "<li".$selected."><a href=\"http://www.be-you.org/jukebox.php\"  onclick=\"window.open(this.href,'x','150','200','no','yes');return false\">JukeBox</a></li>\n";
   				}
   				elseif ($page_name == "webchat") {
   					(is_page($page_id))?$selected = ' class="selected"':$selected='';
   					echo "<li".$selected."><a href=\"".get_page_link($page_id)."\"><b>Web Chat</b></a></li>\n";
   				}
   				elseif ($page_name == "about_short") {
					/*ignore*/
				}
				elseif ($page_name == "galleria") {
   					(is_page($page_id))?$selected = ' class="selected"':$selected='';
   					echo "<li".$selected."><a href=\"http://www.be-you.org/wp-content/plugins/falbum/wp/album.php\">Galleria</a></li>\n";
   				}

           	 	else {
            		(is_page($page_id))?$selected = ' class="selected"':$selected='';
            		echo "<li".$selected."><a href=\"".get_page_link($page_id)."\">$page_title</a></li>\n";
            	}
    		}
    	}
		?>
	</ul>
    </div>
	
    <div id="photos">
    <?php get_flickrRSS(); ?>
    </div>