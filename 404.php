<?php get_header(); ?>

<?php get_sidebar(); ?>

<div id="content">

	<h2>Error 404 &ndash; File not Found</h2>
	
	<p>Sorry, but the page you were looking for could not be found.</p>
	
</div>
<?php get_footer(); ?>