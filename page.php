<?php get_header(); ?>
	
<?php get_sidebar(); ?>
	
    <div id="content">
	
	<?php if (have_posts()) : ?>
		
		<?php while (have_posts()) : the_post(); ?>
			
			<h1><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a><?php if(function_exists('nw_noteworthyIcon')) { nw_noteworthyIcon(); } ?></h1>
			<?php the_content('Read the rest of this entry &raquo;'); ?>
			
		<?php endwhile; ?>
		
	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>

	<?php endif; ?>

<?php get_footer(); ?>
