<?php
function BY_get_pages($with_content = '')
{
	global $wpdb;
    $query = "SELECT ID, post_title, post_name FROM " . $wpdb->posts . " WHERE post_status='static' ORDER BY menu_order ASC";
	if ($with_content == "with_content") {
	   $query = "SELECT ID, post_title,post_name, post_content FROM " . $wpdb->posts . " WHERE post_status='static' ORDER BY menu_order ASC";
	}
	return $wpdb->get_results($query);
}
?>